import React from "react";
import { Layout } from "antd";
import { Input } from "antd";
import shallowCompare from "react-addons-shallow-compare";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";

const { Footer } = Layout;

interface IMessageDetailsToPost {
  message: string;
  channel_id: string;
  props: { username: string };
  user_id: string;
}

interface IFooterBarState {
  message: string;
}

interface ICreatePost {
  channelId: string;
}

interface IFooterBarProps {
  channelId: string;
  userId: string;
  userName: string;
  createPost: (mesageDetailsToPost: IMessageDetailsToPost) => { data: boolean };
  getChannelMessages: (id: string) => void;
}

class FooterBar extends React.Component<IFooterBarProps, IFooterBarState> {
  state: IFooterBarState = {
    message: " "
  };

  // update only when props or state changes
  shouldComponentUpdate(
    nextProps: IFooterBarProps,
    nextState: IFooterBarState
  ): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      message: event.target.value
    });
  };

  handleKeyDown = async (event: any) => {
    if (event.key === "Enter") {
      this.setState({
        message: " "
      });

      let result = await this.props.createPost({
        message: this.state.message,
        channel_id: this.props.channelId,
        user_id: this.props.userId,
        props: { username: this.props.userName }
      });
      console.log("result");

      this.props.getChannelMessages(this.props.channelId);
    }
  };

  render() {
    console.log("in footer");
    return (
      <Footer>
        <Input
          placeholder="Enter Your Message"
          value={this.state.message}
          onChange={this.handleOnChange}
          onKeyDown={this.handleKeyDown}
        />
      </Footer>
    );
  }
}

export default FooterBar;
