import React from "react";
import { Layout } from "antd";
import Sider from "./Sider";
import Header from "./Header";
import Display from "./Content";
import Footer from "./Footer";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
//@ts-ignore
import { getChannels } from "mattermost-redux/actions/channels";
//@ts-ignore
import { getPosts } from "mattermost-redux/actions/posts";
//@ts-ignore
import { createPost } from "mattermost-redux/actions/posts";
import shallowCompare from "react-addons-shallow-compare";
import _ from "lodash";

interface IMessageDetailsToPost {
  message: string;
  channel_id: string;
  props: { username: string };
  user_id: string;
}
interface IMessageDetails {
  message: string;
  messageBy: string;
  messageId: string;
}

interface IChannelDetails {
  id: string;
  name: string;
}

interface IChatPageProps {
  profiles: {
    [key: string]: {
      username: string;
    };
  };
  currentUserId: string;
  teamDetails: {
    [key: string]: {
      display_name: string;
    };
  };

  channelDetails: {
    [key: string]: {
      id: string;
      name: string;
    };
  };

  getPosts: (
    id: string
  ) => {
    data: {
      order: string[];
      posts: {
        [key: string]: {
          message: string;
          props: {
            username: string;
          };
        };
      };
    };
  };

  getChannels: (id: string) => { data: { name: string }[] };

  createPost: (
    messageDetailsToPost: IMessageDetailsToPost
  ) => { data: boolean };
}

interface IChatPageState {
  messages: IMessageDetails[];
  channelId: string;
  channelsData: IChannelDetails[];
  currentUserId: string;
  currentUserName: string;
  displayName: string;
}

class chatPage extends React.Component<IChatPageProps, IChatPageState> {
  currentUserId: string = " ";
  currentTeamId: string = " ";
  currentUserName: string = " ";
  currentDisplayName: string = " ";
  channelDetails: IChannelDetails[] = [];

  constructor(props: IChatPageProps) {
    super(props);
    this.state = {
      messages: [],
      channelId: " ",
      channelsData: [],
      currentUserName: " ",
      currentUserId: " ",
      displayName: " "
    };
  }
  componentDidMount() {
    console.log("it is mounted");
  }

  componentWillMount() {
    console.log("Component will mount", this.state);
  }

  getChannelMessages = async (id: string) => {
    let result = await this.props.getPosts(id);
    let orderOfMessages = _.get(result, "data.order", []);

    let messageDetails: IMessageDetails[] = orderOfMessages.map(
      (messsageId: string) => {
        return {
          message: result.data.posts[messsageId].message,
          messageBy: result.data.posts[messsageId].props.username,
          messageId: messsageId
        };
      }
    );

    this.setState(() => {
      return {
        messages: messageDetails,
        channelId: id,
        currentUserId: this.currentUserId,
        displayName: this.currentDisplayName,
        channelsData: this.channelDetails,
        currentUserName: this.currentUserName
      };
    });
  };

  componentDidUpdate(prevProps: IChatPageProps, prevState: IChatPageState) {
    this.currentUserId = this.props.currentUserId;

    this.currentUserName = _.get(
      this.props.profiles,
      [this.currentUserId, "username"],
      " "
    );

    this.currentTeamId = _.get(_.keys(this.props.teamDetails), "0", " ");
    this.currentDisplayName = _.get(
      this.props.teamDetails,
      [this.currentTeamId, "display_name"],
      " "
    );

    if (this.currentTeamId !== " " && this.state.channelsData.length === 0) {
      (async () => {
        debugger;
        let channelsData = await this.props.getChannels(this.currentTeamId);

        this.channelDetails = channelsData.data.map(
          (eachChannelDetail: any) => {
            return {
              id: eachChannelDetail.id,
              name: eachChannelDetail.name
            };
          }
        );

        this.getChannelMessages(this.channelDetails[0].id);

        debugger;
      })();
    }
  }

  shouldComponentUpdate(
    nextProps: IChatPageProps,
    nextState: IChatPageState
  ): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    console.log("in chat page", this.props);
    return (
      <div className="App">
        <Layout>
          <Sider
            currentUserName={this.state.currentUserName}
            currentDisplayName={this.state.displayName}
            channelDetails={this.state.channelsData}
            getChannelMessages={this.getChannelMessages}
          />

          <Layout>
            <Header />

            <Display messageDetails={this.state.messages} />
            <Footer
              channelId={this.state.channelId}
              userId={this.state.currentUserId}
              userName={this.state.currentUserName}
              createPost={this.props.createPost}
              getChannelMessages={this.getChannelMessages}
            />
          </Layout>
        </Layout>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    // entity: state.entities
    currentUserId: state.entities.users.currentUserId,
    profiles: state.entities.users.profiles,
    // allDetails: state,
    teamDetails: state.entities.teams.teams,
    channelDetails: state.entities.channels.channels
  };
};

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannels,
      getPosts,
      createPost
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(chatPage);
