import React from "react";
import { Layout, Menu, Icon } from "antd";
import { bindActionCreators } from "redux";
//@ts-ignore
import { getChannelMember } from "mattermost-redux/actions/channels";
import shallowCompare from "react-addons-shallow-compare";

import { connect } from "react-redux";
import { Spin } from "antd";

const { Sider } = Layout;

interface IChannelDetails {
  id: string;
  name: string;
}

interface ISiderProps {
  currentUserName: string;
  currentDisplayName: string;
  channelDetails: IChannelDetails[];
  getChannelMessages: (id: string) => void;
  getChannelMember: (channelId: string, userId: string) => {};
}

class SiderBar extends React.Component<ISiderProps, {}> {
  // handleClick(channelName: string) {}

  // handleNewChannelOnClick = async () => {
  //   // <PopUp />;
  // };

  handleOnChannelClick(id: string) {
    this.props.getChannelMessages(id);
  }

  shouldComponentUpdate(nextProps: ISiderProps, nextState: {}): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    console.log("in sider");
    return (
      <Sider
        style={{
          height: "100vh",
          background: "grey",
          color: "blue",
          overflow: "scroll"
        }}
      >
        <Menu
          theme="dark"
          mode="inline"
          defaultSelectedKeys={["10"]}
          style={{ background: "grey", color: "blue" }}
        >
          <Menu.Item key="1" style={{ color: "White" }}>
            <span className="nav-text" style={{ textAlign: "left" }}>
              {this.props.currentDisplayName !== " " ? (
                <div>
                  <p>
                    {this.props.currentDisplayName}

                    {`@${this.props.currentUserName}`}
                  </p>
                </div>
              ) : (
                <h1></h1>
              )}
            </span>
          </Menu.Item>
          <Menu.Item key="2" style={{ color: "White", paddingTop: "5px" }}>
            <button
              style={{
                background: "green",
                border: "1px solid green",
                textAlign: "center"
              }}
            >
              New Channel +
            </button>
          </Menu.Item>

          {this.props.channelDetails.length > 0 ? (
            this.props.channelDetails.map(
              (eachChannelDetails: IChannelDetails) => {
                return (
                  <Menu.Item
                    key={eachChannelDetails.id}
                    style={{ color: "White" }}
                  >
                    <div
                      onClick={e =>
                        this.handleOnChannelClick(eachChannelDetails.id)
                      }
                    >
                      <Icon type="pie-chart" />
                      <span>{eachChannelDetails.name}</span>
                    </div>
                  </Menu.Item>
                );
              }
            )
          ) : (
            <Menu.Item>
              <Spin tip="Loading..." style={{ color: "red" }}></Spin>
            </Menu.Item>
          )}
        </Menu>
      </Sider>
    );
  }
}

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      getChannelMember
    },
    dispatch
  );
export default connect(
  null,
  mapDispatchToProps
)(SiderBar);
