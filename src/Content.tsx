import React from "react";
import { Layout } from "antd";
import { bindActionCreators } from "redux";

import { Card } from "antd";

import { connect } from "react-redux";

//@ts-ignore
import { createPost } from "mattermost-redux/actions/posts";
import shallowCompare from "react-addons-shallow-compare";
const { Content } = Layout;

interface IMessageDetails {
  message: string;
  messageBy: string;
  messageId: string;
}

interface IContentProps {
  messageDetails: IMessageDetails[];
}

class Display extends React.Component<IContentProps, {}> {
  shouldComponentUpdate(nextProps: IContentProps, nextState: {}): boolean {
    return shallowCompare(this, nextProps, nextState);
  }
  render() {
    console.log("in content");
    return (
      <Content style={{ alignItems: "left" }}>
        <div
          id="messages"
          style={{
            width: "auto",
            height: "80vh",
            overflow: "auto"
          }}
        >
          {this.props.messageDetails.length > 0 ? (
            this.props.messageDetails
              .reverse()
              .map((currentMessageDetails: IMessageDetails, index: number) => {
                return (
                  <div
                    key={currentMessageDetails.messageId}
                    style={{ background: "#ECECEC", padding: "5px" }}
                  >
                    <Card
                      title={currentMessageDetails.messageBy}
                      bordered={false}
                      style={{ width: "auto" }}
                    >
                      <p>{currentMessageDetails.message}</p>
                    </Card>
                  </div>
                );
              })
          ) : (
            <div
              style={{
                width: "inherit",
                height: "inherit",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <h1>you are not a member of this channel</h1>
            </div>
          )}
        </div>
      </Content>
    );
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return bindActionCreators(
    {
      createPost
    },
    dispatch
  );
};

export default connect(
  null,
  mapDispatchToProps
)(Display);
