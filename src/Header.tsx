import React from "react";
import { Layout } from "antd";
import { Button } from "antd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
//@ts-ignore
import { logout } from "mattermost-redux/actions/users";
import shallowCompare from "react-addons-shallow-compare";
const { Header } = Layout;

interface IHeaderProps {
  logout: () => { data: boolean };
}

class HeaderBar extends React.Component<IHeaderProps, {}> {
  handleOnClick = async () => {
    const data = await this.props.logout();
    if (data.data === true) {
      window.location.assign("/");
    } else {
      alert("some error");
    }
  };

  shouldComponentUpdate(nextProps: IHeaderProps, nextState: {}): boolean {
    return shallowCompare(this, nextProps, nextState);
  }

  render() {
    console.log("in header");
    return (
      <Header
        style={{
          color: "white",
          textAlign: "left",
          position: "relative",
          background: "white"
        }}
      >
        <Button
          type="danger"
          style={{
            float: "right",
            position: "absolute",
            bottom: "0",
            right: "0"
          }}
          onClick={this.handleOnClick}
        >
          Log Out
        </Button>
      </Header>
    );
  }
}

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      logout
    },
    dispatch
  );

export default connect(
  null,
  mapDispatchToProps
)(HeaderBar);
